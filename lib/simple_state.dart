library simple_state;

import 'dart:async';
export 'src/simple_builder.dart';
export 'src/simple_listener.dart';

class SimpleState {

  SimpleState._();

  static final Map<String, StreamController> _map = {};
  static final Map<String, dynamic> _mapCurrentData = {};

  static clean() {
    _map.clear();
    _mapCurrentData.clear();
  }

  static getCurrentData(String tag) {
    return _mapCurrentData[tag];
  }

  static create<T>(String tag, StreamController streamController, {T? data}) {
    if (!_map.containsKey(tag)) {
      _map[tag] = streamController;
    }
    if (data != null) {
      notify(tag, data);
    }
  }

  static StreamController? get(String tag) {
    return _map[tag];
  }

  static notify<T>(String tag, T data) {
    _mapCurrentData[tag] = data;
    _map[tag]?.add(data);
  }

  static delete(String tag) {
    _map.remove(tag);
  }
}
