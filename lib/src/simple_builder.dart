import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:simple_state/simple_state.dart';

class SimpleBuilder<T> extends StatefulWidget {

  final String tag;
  final Function builder;
  final T? initData;

  const SimpleBuilder({super.key, required this.tag, required this.builder, this.initData});

  @override
  State<StatefulWidget> createState() {
    return _SimpleBuilderState<T>();
  }
}

class _SimpleBuilderState<T> extends State<SimpleBuilder> {

  T? data;

  StreamSubscription? subscription;

  @override
  void initState() {
    SimpleState.create<T>(widget.tag, StreamController<T>.broadcast(), data: widget.initData);
    data = widget.initData ?? SimpleState.getCurrentData(widget.tag);
    subscription = SimpleState.get(widget.tag)?.stream.listen((event) {
      setState(() {
        data = event;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder(data);
  }
}