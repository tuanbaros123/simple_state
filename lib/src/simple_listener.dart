import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:simple_state/simple_state.dart';

class SimpleListener<T> extends StatefulWidget {

  final String tag;
  final Widget? child;
  final Function listener;

  const SimpleListener({super.key, required this.tag, this.child, required this.listener});

  @override
  State<StatefulWidget> createState() {
    return _SimpleBuilderState<T>();
  }
}

class _SimpleBuilderState<T> extends State<SimpleListener> {

  StreamSubscription? subscription;

  @override
  void initState() {
    SimpleState.create<T>(widget.tag, StreamController<T>.broadcast());
    subscription = SimpleState.get(widget.tag)?.stream.listen((event) {
      widget.listener.call(event);
    });
    super.initState();
  }

  @override
  void dispose() {
    subscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child ?? const SizedBox();
  }
}